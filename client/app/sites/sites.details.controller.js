'use strict';

angular.module('moodAppBackendApp')
  .controller('SitesDetailsCtrl', function ($scope, SitesService, $stateParams, $http, $window) {

    $scope.init = function() {
      console.log('sites.init : ' + $stateParams.id);
      console.log("D3: " + JSON.stringify($window.d3) );


      $scope.groupCode = $stateParams.id;
      //$scope.data = ChartService.generateData();
      //$scope.data = ChartService.mockData();
      //$scope.data = ChartService.getData();

      SitesService.getSitesList($scope.groupCode, function(data) {
        console.log('getSitesList()=' + data);
        $scope.siteList = data.sites;
      });

      SitesService.getChordData($scope.groupCode, function(chordData) {
        //$scope.chordData = chordData;
        $scope.dependencyWheelChart = $window.d3.chart.dependencyWheel()
          .width(400)    // also used for height, since the wheel is in a a square
          .margin(100)   // used to display package names
          .padding(.02); // separating groups in the wheel;
        $window.d3.select('#chart_placeholder')
          .datum(chordData)
          .call($scope.dependencyWheelChart);
      });

      //$scope.chordData = {
      //  packageNames: ['PL', 'FR', 'US'],
      //  matrix: [
      //    [10975,  5871, 8916],
      //    [ 1951, 10048, 2060],
      //    [ 8010, 16145, 8090]]
      //};

      //$scope.dependencyWheelChart = $window.d3.chart.dependencyWheel()
      //  .width(400)    // also used for height, since the wheel is in a a square
      //  .margin(70)   // used to display package names
      //  .padding(.02); // separating groups in the wheel;
      //$window.d3.select('#chart_placeholder').datum( $scope.chordData ).call($scope.dependencyWheelChart);
    };

    $scope.addGroup = function(form) {
      console.log('addGroup()');

      $scope.submitted = true;

      if(form.$valid) {
        $http.post('/api/groups/' + $scope.groupCode + '/sites', {
          parentCode: $scope.groupCode,
          siteName: $scope.siteName
        }).then(function() {
          $scope.message = 'Site has been created.';
          $scope.errorMessage = '';
          console.log($scope.message);

          // update site list view
          SitesService.getSitesList($scope.groupCode, function(data) {
            console.log('getSitesList()=' + data);
            $scope.siteList = data.sites;
            $scope.siteName = '';
          });

        }, function() {
          $scope.message = '';
          $scope.errorMessage = 'Site has not been created.';
          console.log($scope.message);
        });
        SitesService.getSitesList(function(data) {
          console.log('getSitesList()=' + data);
          $scope.siteList = data.sites;
        });
      }

    };

    $scope.resetForm = function(form) {
      console.log('resetForm()');

      $scope.submitted = false;
      $scope.message = '';
      $scope.groupCode = '';
      $scope.siteName = '';
    };

  });
