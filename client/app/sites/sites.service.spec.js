'use strict';

describe('Service: sitesService', function () {

  // load the service's module
  beforeEach(module('moodAppBackendApp'));

  // instantiate service
  var sitesService;
  beforeEach(inject(function (_sitesService_) {
    sitesService = _sitesService_;
  }));

  it('should do something', function () {
    expect(!!sitesService).toBe(true);
  });

});
