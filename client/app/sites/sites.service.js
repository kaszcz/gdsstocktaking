'use strict';

angular.module('moodAppBackendApp')
  .factory('SitesService', function ($location, $rootScope, $http) {
    // Service logic

    // Public API here
    return {

      getChordData: function(groupCode, callback) {
        console.log('getChordData() for ' + groupCode);
        $http.get('/api/relationships/' + groupCode).
          success(function(data, status, headers, config) {
            console.log(data);
            //var dataTrans = {
            //  packageNames: ['PL', 'FR', 'US'],
            //  matrix: [
            //    [10975,  5871, 8916],
            //    [ 1951, 10048, 2060],
            //    [ 8010, 16145, 8090]]
            //};
            //console.log(dataTrans);
            callback(data);
          }).
          error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            return null;
          });
      },

      getGroupList: function(callback) {
        console.log('getSitesGroupList');
        $http.get('/api/groups/').
          success(function(groupList, status, headers, config) {
            console.log('getSitesGroupList/success');
            // this callback will be called asynchronously
            // when the response is available
            callback(groupList);
          }).
          error(function(data, status, headers, config) {
            console.log('getSitesGroupList/error');
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            var dataTrans = [
              {
                "key": "Mobility",
                "color": "#FF7F0E",
                "values": []
              }
            ];
            callback(dataTrans);
            //return null;
          });
      },

      getSitesList: function(groupId, callback) {
        console.log('getSitesList');

        $http.get('/api/groups/' + groupId + '/sites').
          success(function(siteList, status, headers, config) {
            console.log('getSitesList/success');
            // this callback will be called asynchronously
            // when the response is available
            callback(siteList);
          }).
          error(function(data, status, headers, config) {
            console.log('getSitesList/error');
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            var dataTrans = [
              {
                sites: [
                  { code: 1, name: "PL"},
                  { code: 2, name: "FR"},
                  { code: 3, name: "US"}
                ]
              }
            ];
            callback(dataTrans);
            //return null;
          });

      }

    };
  });
