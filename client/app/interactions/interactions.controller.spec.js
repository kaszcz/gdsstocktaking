'use strict';

describe('Controller: InteractionsCtrl', function () {

  // load the controller's module
  beforeEach(module('moodAppBackendApp'));

  var InteractionsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    InteractionsCtrl = $controller('InteractionsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
