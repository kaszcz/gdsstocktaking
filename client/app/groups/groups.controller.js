'use strict';

angular.module('moodAppBackendApp')
  .controller('GroupsCtrl', function ($scope, $http, $log, $timeout) {
    var randomString = Math.random().toString(36).slice(-8);

    $scope.message = '';
    $scope.errorMessage = '';
    $scope.groupCode = randomString;
    $scope.groupName = $scope.groupCode;

    $scope.addGroup = function(form) {
      console.log('addGroup()');

      $scope.submitted = true;

      if(form.$valid) {
        $http.post('/api/groups', {
          name: $scope.groupName,
          code: $scope.groupCode
        }).then(function() {
          $scope.message = 'Group has been created.';
          $scope.errorMessage = '';
          console.log($scope.message);
          //$timeout(function(){ $scope.message = ''}, 2000);
        }, function() {
          $scope.message = '';
          $scope.errorMessage = 'Group has not been created.';
          console.log($scope.message);
        });
      }

      $scope.resetForm = function(form) {
        console.log('resetForm()');

        $scope.submitted = false;
        $scope.message = '';
        $scope.groupCode = Math.random().toString(36).slice(-8);
        $scope.groupName = $scope.groupCode;
      }


    };
  });
