'use strict';

angular.module('moodAppBackendApp')
  .factory('ChartService', function ChartServiceFactory($location, $rootScope, $http, User, $cookieStore, $q) {
    /* Inspired by Lee Byron's test data generator. */
    function stream_layers(n, m, o) {
      if (arguments.length < 3) o = 0;
      function bump(a) {
        var x = 1 / (.1 + Math.random()),
          y = 2 * Math.random() - .5,
          z = 10 / (.1 + Math.random());
        for (var i = 0; i < m; i++) {
          var w = (i / m - y) * z;
          a[i] += x * Math.exp(-w * w);
        }
      }
      return d3.range(n).map(function() {
        var a = [], i;
        for (i = 0; i < m; i++) a[i] = o + o * Math.random();
        for (i = 0; i < 5; i++) bump(a);
        return a.map(stream_index);
      });
    }
    /* Another layer generator using gamma distributions. */
    function stream_waves(n, m) {
      return d3.range(n).map(function(i) {
        return d3.range(m).map(function(j) {
          var x = 20 * j / m - i / 3;
          return 2 * x * Math.exp(-.5 * x);
        }).map(stream_index);
      });
    }
    function stream_index(d, i) {
      return {x: i, y: Math.max(0, d)};
    }

    //translate data from server side to chart
    function transformData(data) {
      var dataTrans = [
        {
          "key": 'Team Average',
          "color": "#FF7F0E",
          "values": []
        },
        {
          "key": 'People',
          "values": []
        }
      ];

      for (var i = 1, len = data.moodList.length; i <= len; i++) {
        var moodDate = new Date(data.moodList[i - 1]._id).toISOString().slice(0, 10); // convert to format YYYY-MM-DD
        //console.log(moodDate);

        // create new mood bar
        var newMoodItem = {
          x: i,
          y: data.moodList[i - 1].avgMoodLevel,
          label: moodDate
        };
        dataTrans[0].values.push(newMoodItem);

        var newPeopleItem = {
          x: i,
          y: data.moodList[i - 1].count,
          label: moodDate
        };
        dataTrans[1].values.push(newPeopleItem);

      }
      return dataTrans;
    }; //transformData

    return {
      /* Random Data Generator (took from nvd3.org) */
      generateData: function() {
        return stream_layers(3,50+Math.random()*50,.1).map(function(data, i) {
          return {
            key: 'Stream' + i,
            values: data
          };
        });
      },

      /* Mock Data */
      mockData: function() {
        return [
          {
            "key": "Mobility",
            "color": "#FF7F0E",
            "values": [
              {
                "label": "1",
                "x" : 1,
                "y" : 10
              },
              {
                "label": "2",
                "x" : 2,
                "y" : 20
              }
            ]
          }
        ]
      },

      getAllData: function(groupCode, callback) {
        console.log('getData() for ' + groupCode);

        $http.get('/api/moods/' + groupCode).
          success(function(data, status, headers, config) {
            var dataTrans = transformData(data);
            callback(dataTrans);
          }).
          error(function(data, status, headers, config) {
            return null;
          });
      }, //getAllData

      getOneWeekData: function(groupCode, callback) {
        console.log('getWeekData() for ' + groupCode);
        $http.get('/api/moods/' + groupCode + "/week/current").
          success(function(data, status, headers, config) {
            var dataTrans = transformData(data);
            callback(dataTrans);
          }).
          error(function(data, status, headers, config) {
            return null;
          });
      }, //getOneWeekData

      getOneMonthData: function(groupCode, callback) {
        console.log('getWeekData() for ' + groupCode);
        $http.get('/api/moods/' + groupCode + "/month/current").
          success(function(data, status, headers, config) {
             var dataTrans = transformData(data);
            callback(dataTrans);
          }).
          error(function(data, status, headers, config) {
            return null;
          });
      }, //getOneMonthData

      getThreeMonthsData: function(groupCode, callback) {
        console.log('getWeekData() for ' + groupCode);
        $http.get('/api/moods/' + groupCode + "/month/3m").
          success(function(data, status, headers, config) {
            var dataTrans = transformData(data);
            callback(dataTrans);
          }).
          error(function(data, status, headers, config) {
            return null;
          });
      }, //getThreeMonthsData

      getTwelveMonthsData: function(groupCode, callback) {
        console.log('getWeekData() for ' + groupCode);
        $http.get('/api/moods/' + groupCode + "/month/12m").
          success(function(data, status, headers, config) {
            var dataTrans = transformData(data);
            callback(dataTrans);
          }).
          error(function(data, status, headers, config) {
            return null;
          });
      }, //getTwelveMonthsData

      getGroupList: function(callback) {
        console.log('getGroupList');
        $http.get('/api/groups/').
          success(function(groupList, status, headers, config) {
            callback(groupList);
          }).
          error(function(data, status, headers, config) {
            var dataTrans = [
              {
                "key": "Mobility",
                "color": "#FF7F0E",
                "values": []
              }
            ];
            callback(dataTrans);
            //return null;
          });
      }

    }; // return

  });
