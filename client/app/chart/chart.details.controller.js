'use strict';

angular.module('moodAppBackendApp')
  .controller('ChartDetailsCtrl', function ($scope, ChartService, $stateParams) {

    $scope.init = function() {
      console.log('chart.init : ' + $stateParams.id);

      //$scope.data = ChartService.generateData();
      //$scope.data = ChartService.mockData();
      //$scope.data = ChartService.getData();

      var groupCode = $stateParams.id;

      ChartService.getAllData(groupCode, function(data) {

        console.log('getData()=' + data);
        $scope.data = data;

        $scope.options = {
          chart: {
            type: 'multiBarChart',
            height: 500,
            margin: {
              top: 20,
              right: 20,
              bottom: 60,
              left: 45
            },
            //tooltip: function(key, y, e, graph) {
            //  return '' + e + ' at ' + y;
            //},
            clipEdge: true,
            staggerLabels: true,
            transitionDuration: 500,
            stacked: false,
            showControls: false,
            xAxis: {
              axisLabel: 'Date',
              axisLabelDistance: 40,
              showMaxMin: false,
              tickFormat: function (d) {
                //return d3.format(',f')(d);
                //var dx = $scope.data[0].values[d].x;
                //console.log(label + ', ' + dx);
                var label = $scope.data[0].values[d-1].label?$scope.data[0].values[d-1].label:d;
                console.log('label=' + label);
                return label;

              }
            },
            yAxis: {
              axisLabel: 'Mood Level',
              axisLabelDistance: 40,
              tickFormat: function (d) {
                return d3.format(',.0f')(d);
              }
            }
          }
        }
      });


    };



  });
