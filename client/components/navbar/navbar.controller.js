'use strict';

angular.module('moodAppBackendApp')
  .controller('NavbarCtrl', function ($scope, $location, Auth) {
    //$scope.menu = [{
    //  'title': 'Groups',
    //  'link': '/groups'
    //}, {
    //  'title': 'Chart',
    //  'link': '/chart'
    //  },
    //];
    $scope.menu = [
      //{
      //  'title': 'Groups',
      //  'link': '/groups'
      //}
    ];

    $scope.appVersion = "0.1.0";

    $scope.isCollapsed = true;
    $scope.isLoggedIn = Auth.isLoggedIn;
    $scope.isAdmin = Auth.isAdmin;
    $scope.getCurrentUser = Auth.getCurrentUser;

    $scope.logout = function() {
      Auth.logout();
      $location.path('/login');
    };

    $scope.isActive = function(route) {
      return route === $location.path();
    };
  });
