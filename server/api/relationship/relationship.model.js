'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var findOrCreate = require('mongoose-findorcreate');

var RelationshipSchema = new Schema({
  groupCode: { type: String },
  siteCodeTo: { type: String },
  timestamp: { type: Date },
  uuid: { type: String },
  relationshipCount: { type: Number }
});

RelationshipSchema.plugin(findOrCreate);

module.exports = mongoose.model('Relationship', RelationshipSchema);
