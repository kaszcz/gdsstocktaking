'use strict';

var _ = require('lodash');
var Group = require('./group.model');

// Get list of groups
exports.index = function(req, res) {
  Group.find({parentCode: {$exists: false} }, function (err, groups) {
    if(err) { return handleError(res, err); }
    return res.json(200, groups);
  });
};

// Get a single group
exports.show = function(req, res) {
  //Group.findById(req.params.id, function (err, group) {
  Group.findOne({code: req.params.id}, function (err, group) {
    if(err) { return handleError(res, err); }
    if(!group) { return res.send(404); }
    return res.json(group);
  });
};

// Creates a new group in the DB.
exports.create = function(req, res) {
  if(!req.body.code) { return res.json(404, {"notifications": "GROUP_CODE_NOT_FOUND"}); }

  Group.create(req.body, function(err, group) {
    if(err) { return handleError(res, err); }
    return res.json(201, group);
  });
};

// POST /api/groups/abc123/sites
//{
//  "siteName": "Mobility",
//  "parentCode":"abc123"
//}
exports.createSite = function(req, res) {
  if(!req.params.id) { return res.json(404, {"notifications": "GROUP_CODE_NOT_FOUND"}); }

  Group.find({code: req.params.id}, function (err, group) {
    if (err) { return handleError(res, err); }
    if(!group) { return res.send(404,  {"notifications": "GROUP_CODE_NOT_FOUND"}); }

    var transObj = {
      name: req.body.siteName,
      code: req.params.id+ '.' + req.body.siteName,
      parentCode: req.params.id
    };

    Group.create(transObj, function(err, group) {
      if(err) { return handleError(res, err); }
      return res.json(201, group);
    });

  });

};


// Get a single group
exports.showSite = function(req, res) {
  //Group.findById(req.params.id, function (err, group) {
  Group.find({parentCode: req.params.id}, function (err, group) {
    if(err) { return handleError(res, err); }
    if(!group) { return res.send(404); }

    var respObj = {
      sites: group
    };

    return res.json(respObj);
  });
};

// Updates an existing group in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  //Group.findById(req.params.id, function (err, group) {
  Group.find({code: req.params.id}, function (err, group) {
    if (err) { return handleError(res, err); }
    if(!group) { return res.send(404); }
    var updated = _.merge(group, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, group);
    });
  });
};

// Deletes a group from the DB.
exports.destroy = function(req, res) {
  //Group.findById(req.params.id, function (err, group) {
  Group.findOne({code: req.params.id}, function (err, group) {
    if(err) { return handleError(res, err); }
    if(!group) { return res.send(404); }
    group.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
