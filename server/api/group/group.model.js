'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var uniqueValidator = require('mongoose-unique-validator');

var GroupSchema = new Schema({
  name: String,
  code: { type: String, lowercase: true, unique : true, required : true, dropDups: true, index: true },
  parentCode: { type: String, lowercase: true },
  active: Boolean
});

GroupSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Group', GroupSchema);
