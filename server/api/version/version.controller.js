'use strict';

exports.version = function(req, res) {
    return res.status(200).json({"version": "0.8.0"});
};


function handleError(res, err) {
    return res.send(500, err);
};