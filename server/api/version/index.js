'use strict';

var express = require('express');
var controller = require('./version.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', controller.version);

module.exports = router;
