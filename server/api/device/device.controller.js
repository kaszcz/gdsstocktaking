'use strict';

var _ = require('lodash');
var Device = require('./device.model');
var Group = require('../group/group.model');
var moment = require('../../../node_modules/moment/moment');

// Get list of devices
exports.index = function(req, res) {
  Device.find(function (err, devices) {
    if(err) { return handleError(res, err); }
    return res.json(200, devices);
  });
};

// Get a single device
exports.show = function(req, res) {
  Device.findById(req.params.id, function (err, device) {
    if(err) { return handleError(res, err); }
    if(!device) { return res.send(404); }
    return res.json(device);
  });
};

// Creates a new device in the DB.

// To create a device post without uuid, in return you will get uuid
// POST {{rootURL}}/api/devices/
// {
//   "groupCode": "abc123",
// }

// To update lastSyncDate for your device POST with UUID
// POST {{rootURL}}/api/devices/
// {
//   "groupCode": "abc123",
//    "uuid": "5f3546fe-3754-4a20-b918-d515ad2f8aaf"
// }

exports.create = function(req, res) {
  // check if group exists
  Group.findOne({code: req.body.groupCode}, function (err, group) {
    if (err) { return handleError(res, err); }
    if (!group) { return res.send(404); }

    // check if device has uuid assigned already or not
    if(req.body.uuid) {
      // if it has uuid then check if it updated server today
      Device.findOne({uuid: req.body.uuid}, function (err, device) {
        if (err) { return handleError(res, err); }
        if (!device) { return res.send(404); }

        var today = moment().format('YYYY-MM-DD');
        var lastSyncDate = device.lastSyncDate;
        if(lastSyncDate === today) {
          console.log('You\'ve already updated your mood today!');
          return res.send(401, { notifications: 'ALREADY_UPDATED_TODAY' });
        } else {
          console.log('Your mood should be updated!');
          return res.send(200, { notifications: 'NOT_UPDATED_TODAY' });
        }
      });
    } else {
      // create uuid and send it back to device

      var newUUID = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
      });
      var today = moment().format('YYYY-MM-DD');

      var newDevice = {
        groupCode: req.body.groupCode,
        uuid: newUUID,
        lastSyncDate: today
      };
      console.log(newDevice);

      //Device.create(req.body, function(err, device) {
      Device.create(newDevice, function(err, device) {
        if(err) { return handleError(res, err); }
        var respObj = {
          notifications: 'DEVICE_CREATED',
          device: {
            groupCode: device.groupCode,
            uuid: device.uuid,
            lastSyncDate: device.lastSyncDate
          }
        };
        return res.json(201, respObj);
      });
    }

});


};

// Updates an existing device in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Device.findById(req.params.id, function (err, device) {
    if (err) { return handleError(res, err); }
    if(!device) { return res.send(404); }
    var updated = _.merge(device, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, device);
    });
  });
};

// Deletes a device from the DB.
exports.destroy = function(req, res) {
  Device.findById(req.params.id, function (err, device) {
    if(err) { return handleError(res, err); }
    if(!device) { return res.send(404); }
    device.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
