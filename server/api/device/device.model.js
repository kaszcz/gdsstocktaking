'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var DeviceSchema = new Schema({
  groupCode: {type: String, required: true},
  uuid: {type: String, required: true},
  lastSyncDate: {type: String, required: true}
});

module.exports = mongoose.model('Device', DeviceSchema);
