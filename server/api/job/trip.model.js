'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var findOrCreate = require('mongoose-findorcreate');

var TripSchema = new Schema({
    "timestamp": Number,
    "orderNo": Number,
    "jobNo": Number,
    "tripNo":  Number,
    "tripId" : Number,

    //details
    "tripDate": Number,
    "dispatch": Number,
    "arriveAtSite": Number,
    "jobStart": Number,
    "jobComplete": Number,
    "leaveFromSite": Number,
    "backToOffice": Number,
    "drivingHours": Number,
    "drivingMileage": Number,
    "waitingHours": Number,
    "workingHours": Number
});

TripSchema.index({ orderNo: 1, jobNo: 1, tripNo: 1,  tripDate: 1}, { unique: true });

TripSchema.plugin(findOrCreate);

//TripSchema.pre('save', function(next) {
//    var doc = this;
//    doc.timestamp = Math.floor(new Date(yyyy + "-" + mm + "-" + dd).getTime() / 1000);
//    next();
//});

module.exports = mongoose.model('Trip', TripSchema);

function handleError(res, err) {
    return res.send(500, err);
}