'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var findOrCreate = require('mongoose-findorcreate');

var AssistantTripSchema = new Schema({
    //header
    orderNo: Number,
    jobNo: Number,
    tripNo:  Number,

    //details
    assistantId: String,
    tripDate: Number,
    drivingHours: Number,
    drivingMileage: Number,
    waitingHours: Number,
    workingHours: Number
});

AssistantTripSchema.index({ orderNo: 1, jobNo: 1, tripNo: 1,  assistantId: 1}, { unique: true });

AssistantTripSchema.plugin(findOrCreate);

//TripSchema.pre('save', function(next) {
//    var doc = this;
//    doc.timestamp = Math.floor(new Date(yyyy + "-" + mm + "-" + dd).getTime() / 1000);
//    next();
//});

module.exports = mongoose.model('AssistantTrip', AssistantTripSchema);