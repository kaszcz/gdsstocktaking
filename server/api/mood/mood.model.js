'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var findOrCreate = require('mongoose-findorcreate');

var MoodSchema = new Schema({
  groupCode: { type: String, index: true },
  timestamp: { type: Date, index: true },
  uuid: { type: String },
  moodLevel: { type: Number }
});

MoodSchema.plugin(findOrCreate);

module.exports = mongoose.model('Mood', MoodSchema);
