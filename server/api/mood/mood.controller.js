'use strict';

var _ = require('lodash');
var Mood = require('./mood.model');
var Group = require('../group/group.model');
var Device = require('../device/device.model');
var moment = require('moment');

// Get list of moods
exports.index = function(req, res) {
  Mood.find(function (err, moods) {
    if(err) { return handleError(res, err); }
    return res.json(200, moods);
  });
};

// Get mood list for a given group code
//GET {{rootURL}}/api/moods/abc123
exports.show = function(req, res) {

  Group.findOne({code: req.params.id}, function (err, group) {
    if (err) { return handleError(res, err); }
    if (!group) { return res.send(404); }

    Mood.aggregate(
      [
        { $match: { groupCode: req.params.id } },
        {
          $group:
          {
            _id: '$timestamp',
            avgMoodLevel: { $avg: '$moodLevel' },
            count: { $sum: 1 }
          }
        }
      ]
    ).sort('_id')
      .exec(function (err, resp) {
        if (err) return handleError(err);
        var respObj = {
          meta: {
            groupName: group.name,
            recCount: resp.length
          },
          moodList: resp
        };

        console.log(respObj);
        return res.json(respObj);
      });

  });

}; //show

// checks if a value is a number
var isNumber = function (n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
};

// Get mood list for a given group code and week
//GET {{rootURL}}/api/moods/abc123/week/23
//GET {{rootURL}}/api/moods/abc123/week/last
exports.showWeek = function(req, res) {
  console.log("showWeek, id=" + req.params.id + ", week=" + req.params.week_no);

  // get beginning and end dates for last iso Week
  var lastWeekDay, beginDate, endDate, beginDateStr, endDateStr;

  // get the starting day of week
  if (req.params.week_no === "last") {
    // get beginning and end dates for last iso Week
    lastWeekDay = moment().subtract(7, "days");
  } else if(req.params.week_no === "current") {
    lastWeekDay = moment();
  } else if(isNumber(req.params.week_no))  {
    // get beginning and end dates for given iso Week
    console.log("isNumeric");
    lastWeekDay = moment().subtract(req.params.week_no, "weeks");
  } else {
    // assume last week
    lastWeekDay = moment().subtract(7, "days");
  }

  // get beginning and end dates for last iso Week
  beginDate = moment(lastWeekDay).isoWeekday(1);
  endDate = moment(lastWeekDay).isoWeekday(7);
  beginDateStr = beginDate.format("YYYY-MM-DD");
  endDateStr = endDate.format("YYYY-MM-DD");
  console.log("beginDate=" + beginDateStr);
  console.log("endDate=" + endDateStr);

  Group.findOne({code: req.params.id}, function (err, group) {
    if (err) { return handleError(res, err); }
    if (!group) { return res.send(404); }

    Mood.aggregate(
      [
        { $match : {
            groupCode: req.params.id,
            timestamp: {
              $gte: new Date(beginDateStr),
              $lte: new Date(endDateStr)
            }
          }
        },
          { $group: {
            _id: '$timestamp',
            avgMoodLevel: { $avg: '$moodLevel' },
            count: { $sum: 1 }
          }
        }
      ]
    ).sort('_id')
      .exec(function (err, resp) {
        if (err) return handleError(err);

        var respObj = {
          meta: {
            groupName: group.name,
            recCount: resp.length
          },
          moodList: resp
        };

        console.log(respObj);
        return res.json(respObj);
      });

  });

}; //showWeek

// Get mood list for a given group code and week
//GET {{rootURL}}/api/moods/abc123/month/4
//GET {{rootURL}}/api/moods/abc123/month/current
//GET {{rootURL}}/api/moods/abc123/month/3m
exports.showMonth = function(req, res) {
  console.log("showMonth, id=" + req.params.id + ", month_no=" + req.params.month_no);

  // get beginning and end dates for last iso Week
  var startDay, beginDate, endDate, beginDateStr, endDateStr;

  // get the starting day of week
  if (req.params.month_no === "3m") {
    // get beginning and end dates for last 3 months
    startDay = moment();
    beginDate = moment([startDay.year(), startDay.month()]);
    beginDate.subtract(2, "months");
    endDate = moment(startDay).endOf('month');
  } else if (req.params.month_no === "12m") {
    // get beginning and end dates for last 3 months
    startDay = moment();
    beginDate = moment([startDay.year(), startDay.month()]);
    beginDate.subtract(11, "months");
    endDate = moment(startDay).endOf('month');
  } else if(req.params.month_no === "current") {
    // get beginning and end dates for current month
    startDay = moment();
    beginDate = moment([startDay.year(), startDay.month()]);
    endDate = moment(startDay).endOf('month');
  } else if(isNumber(req.params.month_no))  {
    // get beginning and end dates for given iso Week
    console.log("isNumeric");
    startDay = moment();
    beginDate = moment([startDay.year(), req.params.month_no - 1]);
    endDate = moment(beginDate).endOf('month');
  } else {
    // assume current month
    startDay = moment();
    beginDate = moment([startDay.year(), startDay.month()]);
    endDate = moment(startDay).endOf('month');
  }

  beginDateStr = beginDate.format("YYYY-MM-DD");
  endDateStr = endDate.format("YYYY-MM-DD");
  console.log("beginDate=" + beginDateStr);
  console.log("endDate=" + endDateStr);

  Group.findOne({code: req.params.id}, function (err, group) {
    if (err) { return handleError(res, err); }
    if (!group) { return res.send(404); }

    Mood.aggregate(
      [
        { $match : {
          groupCode: req.params.id,
            timestamp: {
              $gte: new Date(beginDateStr),
              $lte: new Date(endDateStr)
           }
         }
        },
        { $group: {
            _id: '$timestamp',
            avgMoodLevel: { $avg: '$moodLevel' },
            count: { $sum: 1 }
          }
        }
      ]
    ).sort('_id')
      .exec(function (err, resp) {
        if (err) return handleError(err);

        var respObj = {
          meta: {
            groupName: group.name,
            recCount: resp.length
          },
          moodList: resp
        };

        console.log(respObj);
        return res.json(respObj);
      });

  });

}; //showWeek


// Creates a new mood in the DB.
// POST
// {{rootURL}}/api/moods/
//{
//  "groupCode": "abc123",
//  "timestamp": "2015-02-24",
//  "moodLevel": 15,
//  "uuid": "5f3546fe-3754-4a20-b918-d515ad2f8aaf"
//}
exports.create = function(req, res) {
  Group.findOne({code: req.body.groupCode}, function (err, group) {
    if(err) { return handleError(res, err); }
    if(!group) { return res.send(404); }

    //Check if your mood has been updated today
    if(req.body.uuid) {
      // if it has uuid then check if it updated server today
      Device.findOne({uuid: req.body.uuid}, function (err, device) {
        if (err) { return handleError(res, err); }
        if (!device) { return res.send(404, { notifications: 'DEVICE_ID_NOT_FOUND' }); }

        Mood.findOrCreate(
          {
            groupCode: req.body.groupCode,
            timestamp: req.body.timestamp,
            uuid: req.body.uuid
          }, function(err, mood, created) {
            if(err) { return handleError(res, err); }

            if(mood._id) { delete mood._id }
            if(mood.__v) { delete mood.__v }

            var moodRes = {
              notifications: created?"CREATED_OK":"UPDATED_OK",
              mood: {
                timeStamp: mood.timeStamp,
                groupCode: mood.groupCode,
                moodLevel: req.body.moodLevel
              },
              kaszcz: "BITBUCKET_TEST_OK"
            };

            //add moodLevel field
            mood.moodLevel = req.body.moodLevel;
            mood.save(function(err) {
              if (err) { return handleError(res, err); }

              // MOOD-90 - update lastSyncDate - start
              var todayDate = moment().isoWeekday(1);
              var todayDateStr = todayDate.format("YYYY-MM-DD");
              device.lastSyncDate = todayDateStr;
              device.save(function(err) {
                if (err) {
                  console.log('lastSyncDate not updated for device=' + device.uuid);
                }
              });
              // MOOD-90 - update lastSyncDate - end

            });

            // Update devices list
            return res.json(201, moodRes);
          });

      });

    } else {
      return res.send(401, { notifications: 'DEVICE_ID_NOT_FOUND' });
    }


  });


};

// Updates an existing mood in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Mood.findById(req.params.id, function (err, mood) {
    if (err) { return handleError(res, err); }
    if(!mood) { return res.send(404); }
    var updated = _.merge(mood, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, mood);
    });
  });
};

// Deletes a mood from the DB.
exports.destroy = function(req, res) {
  Mood.findById(req.params.id, function (err, mood) {
    if(err) { return handleError(res, err); }
    if(!mood) { return res.send(404); }
    mood.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
