'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PrepickSchema = new Schema({
    _id: {type: Number, index: true},
    dealerNo: {type: Number, index: true},
    branchNo: {type: Number, index: true},
    partPrefix: {type: String, index: true},
    partNo: {type: String, index: true},
    location: String,
    quantity: Number
});

// Ensure virtual fields are serialised.
PrepickSchema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model('Prepick', PrepickSchema);
