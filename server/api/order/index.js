'use strict';

var express = require('express');
var controller = require('./order.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', controller.index);
router.get('/:orderNo', controller.show);
router.put('/:orderNo/edit', controller.update);
router.put('/:orderNo/release', controller.releaseOrder);

router.get('/:orderNo/history', controller.showHistory);

router.get('/:orderNo/parts', controller.showParts);
router.put('/:orderNo/parts/:partNo', controller.updatePart);

router.get('/:orderNo/jobs', controller.showJobs);
router.post('/:orderNo/jobs', controller.createJobs);

router.get('/:orderNo/jobs/:jobNo/trips', controller.showTrips);
router.post('/:orderNo/jobs/:jobNo/trips', controller.createTrip);
router.get('/:orderNo/jobs/:jobNo/trips/:tripNo', controller.showTripByNumber);
router.put('/:orderNo/jobs/:jobNo/trips/:tripNo', controller.updateTrip);

router.get('/:orderNo/jobs/:jobNo/trips/:tripNo/assistants', controller.showAssistantTrips);
router.put('/:orderNo/jobs/:jobNo/trips/:tripNo/assistants/:assistantId', controller.updateAssistantTrip);
router.delete('/:orderNo/jobs/:jobNo/trips/:tripNo/assistants/:assistantId', controller.deleteAssistantTrip);

router.get('/:orderNo/photos', controller.showPhotos);
router.post('/:orderNo/photos', controller.createPhoto);
router.get('/:orderNo/photos/:photoId', controller.showPhotoById);
//router.post('/', auth.hasRole('admin'), controller.create);

//router.patch('/:id', auth.hasRole('admin'), controller.update);
//router.delete('/:id', auth.hasRole('admin'), controller.destroy);

module.exports = router;
