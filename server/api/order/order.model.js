'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var OrderSchema = new Schema({
    // header
    _id: Number,
    orderNo: {type: Number, index: true},
    MAT: String,
    orderStatus: String,
    failure: String,
    customerName: String,
    contactNumber: String,
    model: String,
    machineLocation: String,
    machineSN: String,
    createdDate: Number,
    plannedDate: Number,
    closedDate: Number,
    HMR: Number,
    breakDown: Boolean,
    mechanic: String,
    // details
    smsContent: String,
    attentionForMachine: String,
    attentionForCustomer: String,
    contactPerson: String,
    createdDatetime: Number,
    plannedDatetime: Number,
    deliveryDatetime: Number,
    closedDatetime: Number
});

// Duplicate the ID field.
//OrderSchema.virtual('id').get(function() {
//    var id = mongoose.Types.ObjectId(this._id);
//    return parseInt(this._id);
//});

// Ensure virtual fields are serialised.
OrderSchema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model('Order', OrderSchema);

