'use strict';

var config = require('../../config/environment');

var _ = require('lodash');
var moment = require('../../../node_modules/moment/moment');

var Order = require('./order.model');
var History = require('../history/history.model');
var Part = require('../part/part.model');
var Job = require('../job/job.model');
var Trip = require('../job/trip.model');
var AssistantTrip = require('../job/assistant.trip.model');
var Assistant = require('../assistant/assistant.model');
var Photo = require('../photo/photo.model');


exports.version = function(req, res) {
    return res.status(200).json({"version": "0.1.0"});
};

//======================================================================================================================
//===== Orders
//======================================================================================================================
exports.index = function(req, res) {
    var status = req.param('status');
    console.log("status=" + status);

    if(!status) {
        var resObjList = [];

        Order.find(function (err, orders) {
            if(err) { return handleError(res, err); }
            _.forOwn(orders, function(value, key) {
                var resObj = convertToOrderListObject(value);
                resObjList.push(resObj);
            });
            return res.status(200).json(resObjList);
        });
    } else if(status == "active") {
        var resObjList = [];

        Order.find( { $or:[ {orderStatus: 'CRTD'}, {orderStatus: 'REL'} ]},
            function (err, orders) {
                _.forOwn(orders, function(value, key) {
                    var resObj = convertToOrderListObject(value);
                    resObjList.push(resObj);
                });

                if(err) { return handleError(res, err); }
                return res.status(200).json(resObjList);
            });
    } else if(status == "closed") {
        var resObjList = [];

        Order.find({orderStatus: 'TECO'}, function (err, orders) {
            if(err) { return handleError(res, err); }
            _.forOwn(orders, function(value, key) {
                var resObj = convertToOrderListObject(value);
                resObjList.push(resObj);
            });

            return res.status(200).json(resObjList);
        });
    } else {
        return res.json(404, {"message": "Wrong status!"});
    }

};


exports.show = function(req, res) {
    Order.findOne({orderNo: req.params.orderNo}, function (err, order) {
        if (err) { return handleError(res, err); }
        if (!order) { return res.status(404).json({"message": "Order not found!"}); }

        var resObj = convertToOrderDetailObject(order);

        return res.status(200).json(resObj);
    });
}; //show


exports.update = function(req, res) {
    // sanitize input
    if(req.body._id) { delete req.body._id; }

    Order.findOne({orderNo: req.params.orderNo}, function (err, order) {
        if (err) { return handleError(res, err); }
        if(!order) { return res.status(404).json({"message": "Order not found!"}); }

        delete req.body.orderNo;

        var updated = _.merge(order, req.body);
        updated.save(function (err) {
            if (err) { return handleError(res, err); }

            var resObj = convertToOrderDetailObject(order);

            return res.json(200, resObj);
        });
    });
};

exports.releaseOrder = function(req, res) {
    // sanitize input
    if(req.body._id) { delete req.body._id; }

    Order.findOne({orderNo: req.params.orderNo}, function (err, order) {
        if (err) { return handleError(res, err); }
        if(!order) { return res.status(404).json({"message": "Order not found!"}); }

        var updated = order;
        updated.orderStatus = 'REL';

        updated.save(function (err) {
            if (err) { return handleError(res, err); }

            var resObj = convertToOrderDetailObject(order);

            return res.json(200, resObj);
        });
    });
};

function convertToOrderListObject(mongooseObj) {
    var obj = {};
    obj.id = mongooseObj._id;
    obj.orderNo = mongooseObj.orderNo;
    obj.MAT = mongooseObj.MAT;
    obj.orderStatus = mongooseObj.orderStatus;
    obj.failure = mongooseObj.failure;
    obj.customerName = mongooseObj.customerName;
    obj.contactNumber = mongooseObj.contactNumber;
    obj.model = mongooseObj.model;
    obj.machineLocation = mongooseObj.machineLocation;
    obj.machineSN = mongooseObj.machineSN;
    obj.createdDate = mongooseObj.createdDate;
    obj.plannedDate = mongooseObj.plannedDate;
    obj.closedDate = mongooseObj.closedDate;
    obj.HMR = mongooseObj.HMR;
    obj.breakDown = mongooseObj.breakDown;
    obj.mechanic = mongooseObj.mechanic;
    return obj;
}


function convertToOrderDetailObject(mongooseObj) {
    var obj = {};
    obj.id = mongooseObj._id;
    obj.orderNo = mongooseObj.orderNo;
    obj.MAT = mongooseObj.MAT;
    obj.orderStatus = mongooseObj.orderStatus;
    obj.failure = mongooseObj.failure;
    obj.customerName = mongooseObj.customerName;
    obj.contactNumber = mongooseObj.contactNumber;
    obj.model = mongooseObj.model;
    obj.machineLocation = mongooseObj.machineLocation;
    obj.machineSN = mongooseObj.machineSN;
    obj.createdDate = mongooseObj.createdDate;
    obj.closedDate = mongooseObj.closedDate;
    obj.HMR = mongooseObj.HMR;
    obj.breakDown = mongooseObj.breakDown;
    obj.mechanic = mongooseObj.mechanic;
    obj.contactPerson = mongooseObj.contactPerson;
    obj.createdDatetime = mongooseObj.createdDatetime;
    obj.plannedDatetime = mongooseObj.plannedDatetime;
    obj.deliveryDatetime = mongooseObj.deliveryDatetime;
    obj.closedDatetime = mongooseObj.closedDatetime;
    obj.attentionForMachine = mongooseObj.attentionForMachine;
    obj.attentionForCustomer = mongooseObj.attentionForCustomer;
    obj.smsContent = mongooseObj.smsContent;
    return obj;
}

//======================================================================================================================
//===== History
//======================================================================================================================
exports.showHistory = function(req, res) {
    var resObjList = [];

    History.find(function (err, historyItems) {
        if(err) { return handleError(res, err); }
        _.forOwn(historyItems, function(value, key) {
            var resObj = convertToOrderListObject(value);
            resObjList.push(resObj);
        });
        return res.status(200).json(resObjList);
    });
};


//======================================================================================================================
//===== Parts
//======================================================================================================================
exports.showParts = function(req, res) {
    var paramOrderNo = req.param('orderNo');

    var resObjList = [];

    Part.find({orderNo: paramOrderNo}, function (err, parts) {
        if(err) { return handleError(res, err); }
        _.forOwn(parts, function(value, key) {
            var resObj = convertToPartListObject(value);
            resObjList.push(resObj);
        });
        return res.status(200).json(resObjList);
    });
};

exports.updatePart = function(req, res) {
    // sanitize input
    var paramOrderNo = parseInt(req.param('orderNo'));
    var paramPartNumber = req.param('partNo');
    if(!paramOrderNo) { return res.status(404).json({"message": "You need to pass an order number"}); }
    if(!paramPartNumber) { return res.status(404).json({"message": "You need to pass a part number"}); }

    Part.findOne({ orderNo: paramOrderNo, partNumber: paramPartNumber }, function (err, part) {
        if (err) { return handleError(res, err); }
        if(!part) { return res.status(404).json({"message": "Part not found!"}); }

        delete req.body.timestamp;
        var updated = _.merge(part, req.body);
        updated.save(function (err) {
            if (err) { return handleError(res, err); }
            var resObj = convertToPartListObject(part);
            return res.json(200, resObj);
        });
    });
};


function convertToPartListObject(part) {
    var obj = {};

    obj.id = part._id;
    obj.partNumber = part.partNumber;
    obj.description = part.description;
    obj.qty = part.qty;
    obj.IC = part.IC;
    obj.PO = part.PO;
    obj.WMS = part.WMS;
    obj.jobNo = part.jobNo;
    obj.oldSN = part.oldSN;
    obj.newSN = part.newSN;

    return obj;
}
//======================================================================================================================
//===== Jobs
//======================================================================================================================
exports.showJobs = function(req, res) {
    var paramOrderNo = req.param('orderNo');
    var resObjList = [];

    Job.find({orderNo: paramOrderNo}).sort({timestamp: 1}).exec(function(err, jobs) { //{, function (err, jobs) {
        if(err) { return handleError(res, err); }
        _.forOwn(jobs, function(value, key) {
            var resObj = convertToJobListObject(value);
            resObjList.push(resObj);
        });
        return res.status(200).json(resObjList);
    });
};

exports.createJobs = function(req, res) {
    var paramOrderNo = req.param('orderNo');
    if(!paramOrderNo) { return res.status(404).json({"message": "You need to pass an order number"}); }

    Job.findOne({orderNo: paramOrderNo}).sort({timestamp: -1}).exec(function(err, lastJob) {
        if(err) { return handleError(res, err); }

        var newJob = {};
        newJob.orderNo = paramOrderNo;
        newJob.timestamp = Math.floor(Date.now());
        newJob.jobName = req.body.failure;

        if(lastJob) { // order has at least one job
            newJob.jobId = lastJob.jobNo + 1 + Math.floor((Math.random() * 10000) + 1);
            newJob.jobNo = lastJob.jobNo + 10;
        } else { // this is the first job in the order
            newJob.jobId = 0;
            newJob.jobNo = 10;
        }

        Job.create(newJob, function (err, createdJob) {
            if (err) { return handleError(res, err); }
            var resObj = convertToJobListObject(createdJob);
            return res.status(200).json(resObj);
        });

    });

};

function convertToJobListObject(mongooseObj) {
    var obj = {};

    obj.jobId = mongooseObj.jobId;
    obj.timestamp = Math.floor(mongooseObj.timestamp / 1000);
    obj.jobNo = ((mongooseObj.jobNo < 100) ? "00" : "0") + mongooseObj.jobNo;
    obj.jobName = mongooseObj.jobName;

    return obj;
}

//======================================================================================================================
//===== Trips
//======================================================================================================================
exports.showTrips = function(req, res) {
    var paramOrderNo = req.param('orderNo');
    var paramJobNo = req.param('jobNo');
    if(!paramOrderNo) { return res.status(404).json({"message": "You need to pass an order number"}); }
    if(!paramJobNo) { return res.status(404).json({"message": "You need to pass a job number"}); }

    Trip.find({orderNo: paramOrderNo, jobNo: paramJobNo}, {}, { sort: { 'timestamp' : 1 } }, function(err, trips) {
        if(err) { return handleError(res, err); }
        if(!trips || trips.length <= 0) { return res.status(404).json({"message": "Trips not found!"}); }

        var resObjList = [];
        _.forOwn(trips, function(value, key) {
            var resObj = convertToTripListObject(value);
            resObjList.push(resObj);
        });
        return res.status(200).json(resObjList);
    });

};

exports.showTripByNumber = function(req, res) {
    var paramOrderNo = req.param('orderNo');
    var paramJobNo = req.param('jobNo');
    var paramTripNumber = req.param('tripNo');
    if(!paramOrderNo) { return res.status(404).json({"message": "You need to pass an order number"}); }
    if(!paramJobNo) { return res.status(404).json({"message": "You need to pass a job number"}); }
    if(!paramTripNumber) { return res.status(404).json({"message": "You need to pass a trip number"}); }

    Trip.findOne({ orderNo: paramOrderNo, jobNo: paramJobNo, tripNo: paramTripNumber }, function(err, trip) {
        if(err) { return handleError(res, err); }
        if(!trip) {return res.status(404).json({"message": "Trip not found"});}

        var resObj = convertToTripDetailObject(trip);
        return res.status(200).json(resObj);
    });

};

exports.createTrip = function(req, res) {
    var paramOrderNo = req.param('orderNo');
    var paramJobNo = req.param('jobNo');
    if(!paramOrderNo) { return res.status(404).json({"message": "You need to pass an order number"}); }
    if(!paramJobNo) { return res.status(404).json({"message": "You need to pass a job number"}); }

    var todayDate = convertTimestampToDate(moment());
    var reqDate = convertTimestampToDate(moment.unix(req.body.tripDate));

    // find last trip to get tripNo
    Trip.findOne({ orderNo: paramOrderNo, jobNo: paramJobNo}).sort({tripDate: -1}).exec(function(err, lastTrip) {
        if (err) { return handleError(res, err); }

        var newTrip = {};
        newTrip.tripDate = reqDate;

        if(lastTrip) { // job has at least one trip
            newTrip.tripNo = lastTrip.tripNo + 1;
        } else {
            newTrip.tripNo = 1;
        }

        Trip.findOrCreate({orderNo: paramOrderNo, jobNo: paramJobNo, tripDate: reqDate},
            function(err, trip, created) {
                if(err) { return handleError(res, err); }

                var isCreated = {true: "CREATED", false: "NOT_CREATED"};
                console.log(isCreated[created] + " " + JSON.stringify(trip));
                if(!created) { return res.status(200).json(convertToTripDetailObject(lastTrip)); }

                trip.tripNo = newTrip.tripNo;
                trip.tripDate = newTrip.tripDate;
                trip.save(function (err) {
                    if (err) { return handleError(res, err); }
                    var resObj = convertToTripDetailObject(trip);
                    return res.json(200, resObj);
                });
            });


    });

};

exports.updateTrip = function(req, res) {
    // sanitize input
    var paramOrderNo = req.param('orderNo');
    var paramJobNo = req.param('jobNo');
    var paramTripNumber = req.param('tripNo');
    if(!paramOrderNo) { return res.status(404).json({"message": "You need to pass an order number"}); }
    if(!paramJobNo) { return res.status(404).json({"message": "You need to pass a job number"}); }
    if(!paramTripNumber) { return res.status(404).json({"message": "You need to pass a trip number"}); }

    Trip.findOne({ orderNo: paramOrderNo, jobNo: paramJobNo, tripNo: paramTripNumber }, function (err, trip) {
        if (err) { return handleError(res, err); }
        if(!trip) { return res.status(404).json({"message": "Trip not found!"}); }

        delete req.body.timestamp;
        var updated = _.merge(trip, req.body);
        updated.save(function (err) {
            if (err) { return handleError(res, err); }
            var resObj = convertToTripDetailObject(trip);
            return res.json(200, resObj);
        });
    });
};

function convertToTripListObject(mongooseObj) {
    var obj = {};

    obj.tripNo = mongooseObj.tripNo;
    obj.tripDate = mongooseObj.tripDate;

    return obj;
}

function convertToTripDetailObject(mongooseObj) {
    var obj = {};

    //obj.orderNo = mongooseObj.orderNo;
    //obj.jobNo = ((mongooseObj.jobNo < 100) ? "00" : "0") + mongooseObj.jobNo;
    obj.tripNo = mongooseObj.tripNo;
    //obj.tripId = mongooseObj.tripId;

    obj.tripDate = mongooseObj.tripDate;
    obj.dispatch = mongooseObj.dispatch;
    obj.arriveAtSite = mongooseObj.arriveAtSite;
    obj.jobStart = mongooseObj.jobStart;
    obj.jobComplete = mongooseObj.jobComplete;
    obj.leaveFromSite = mongooseObj.leaveFromSite;
    obj.backToOffice = mongooseObj.backToOffice;
    obj.drivingHours = mongooseObj.drivingHours;
    obj.drivingMileage = mongooseObj.drivingMileage;
    obj.waitingHours = mongooseObj.waitingHours;
    obj.workingHours = mongooseObj.workingHours;

    return obj;
}

function convertTimestampToDate(timestamp) {
    timestamp.millisecond(0);
    timestamp.second(0);
    timestamp.minute(0);
    timestamp.hour(0);
    return Math.floor(timestamp / 1000 );
}

//======================================================================================================================
//===== Assistant Trips
//======================================================================================================================

exports.showAssistantTrips = function(req, res) {
    // sanitize input
    var paramOrderNo = req.param('orderNo');
    var paramJobNo = req.param('jobNo');
    var paramTripNumber = req.param('tripNo');
    if(!paramOrderNo) { return res.status(404).json({"message": "You need to pass an order number"}); }
    if(!paramJobNo) { return res.status(404).json({"message": "You need to pass a job number"}); }
    if(!paramTripNumber) { return res.status(404).json({"message": "You need to pass a trip number"}); }

    var assistantsMap = {};

    Assistant.find({}, {},
        function(err, assistants) {
            if(err) { return handleError(res, err); }
            if(!assistants || assistants.length <= 0) { return res.status(404).json({"message": "Assistants not found!"}); }

            // create an assistants map
            _.forOwn(assistants, function(value, key) {
                assistantsMap[value.assistantId] = value.userName;
                console.log(value.assistantId + ", " +  value.userName);
            });

            AssistantTrip.find({orderNo: paramOrderNo, jobNo: paramJobNo, tripNo: paramTripNumber}, {}, { sort: { 'tripDate' : 1 } },
                function(err, assistantTrips) {
                    if(err) { return handleError(res, err); }
                    if(!assistantTrips || assistantTrips.length <= 0) { return res.status(404).json({"message": "Assistant trips not found!"}); }

                    var resObjList = [];
                    _.forOwn(assistantTrips, function(value, key) {
                        var userName = assistantsMap[assistantTrips[key].assistantId];
                        var resObj = convertToAssistantTripListObject(value);
                        resObj.userName = userName;
                        resObjList.push(resObj);
                    });
                    return res.status(200).json(resObjList);
                });

        });



};

exports.updateAssistantTrip = function(req, res) {
    // sanitize input
    var paramOrderNo = parseInt(req.param('orderNo'));
    var paramJobNo = parseInt(req.param('jobNo'));
    var paramTripNumber = parseInt(req.param('tripNo'));
    var paramTripAssistantId = req.param('assistantId').toUpperCase();
    if(!paramOrderNo) { return res.status(404).json({"message": "You need to pass an order number"}); }
    if(!paramJobNo) { return res.status(404).json({"message": "You need to pass a job number"}); }
    if(!paramTripNumber) { return res.status(404).json({"message": "You need to pass a trip number"}); }
    if(!paramTripAssistantId) { return res.status(404).json({"message": "You need to pass an assistant Id"}); }

    AssistantTrip.findOrCreate({orderNo: paramOrderNo, jobNo: paramJobNo, tripNo: paramTripNumber, assistantId: paramTripAssistantId},
        function(err, assistantTrip, created) {
            if(err) { return handleError(res, err); }
            var isCreated = {true: "CREATED", false: "NOT_CREATED"};

            console.log(isCreated[created] + " " + JSON.stringify(assistantTrip));
            //var updated = _.merge(assistantTrip, req.body);

            assistantTrip.drivingHours = req.body.drivingHours;
            assistantTrip.drivingMileage = req.body.drivingMileage;
            assistantTrip.waitingHours = req.body.waitingHours;
            assistantTrip.workingHours = req.body.workingHours;
            assistantTrip.tripDate = req.body.tripDate;
            assistantTrip.save(function (err) {
                if (err) { return handleError(res, err); }
                var resObj = convertToAssistantTripListObject(assistantTrip);
                return res.json(200, resObj);
            });
        });
};

exports.deleteAssistantTrip = function(req, res) {
    // sanitize input
    var paramOrderNo = parseInt(req.param('orderNo'));
    var paramJobNo = parseInt(req.param('jobNo'));
    var paramTripNumber = parseInt(req.param('tripNo'));
    var paramTripAssistantId = req.param('assistantId').toUpperCase();
    if(!paramOrderNo) { return res.status(404).json({"message": "You need to pass an order number"}); }
    if(!paramJobNo) { return res.status(404).json({"message": "You need to pass a job number"}); }
    if(!paramTripNumber) { return res.status(404).json({"message": "You need to pass a trip number"}); }
    if(!paramTripAssistantId) { return res.status(404).json({"message": "You need to pass an assistant Id"}); }

    //Device.findById(req.params.id, function (err, device) {
    //    if(err) { return handleError(res, err); }
    //    if(!device) { return res.send(404); }
    //    device.remove(function(err) {
    //        if(err) { return handleError(res, err); }
    //        return res.send(204);
    //    });
    //});

    AssistantTrip.findOne({ orderNo: paramOrderNo, jobNo: paramJobNo, tripNo: paramTripNumber, assistantId:  paramTripAssistantId},
        function (err, assistantTrip) {
            if (err) { return handleError(res, err); }
            if(!assistantTrip) { return res.status(404).json({"message": "Assistant trip not found!"}); }

            assistantTrip.remove(function(err) {
                if(err) { return handleError(res, err); }
                return res.status(200).json(convertToAssistantTripListObject(assistantTrip));
            });

        });

    //return res.status(404).json({"messsage": "Not implemented yet"});
};


function convertToAssistantTripListObject(mongooseObj) {
    var obj = {};

    obj.tripDate = mongooseObj.tripDate;
    obj.userName = mongooseObj.userName;
    obj.drivingHours = mongooseObj.drivingHours;
    obj.drivingMileage = mongooseObj.drivingMileage;
    obj.waitingHours = mongooseObj.waitingHours;
    obj.workingHours = mongooseObj.workingHours;
    obj.assistantId = mongooseObj.assistantId;

    return obj;
}

//======================================================================================================================
//===== Photos
//======================================================================================================================

exports.showPhotos = function(req, res) {
    var paramOrderNo = parseInt(req.param('orderNo'));
    if(!paramOrderNo) { return res.status(404).json({"message": "You need to pass an order number"}); }

    Photo.find({ orderNo: paramOrderNo}, function(err, photos) {
        if(err) { return handleError(res, err); }
        if(!photos) {return res.status(404).json({"message": "Photo not found"});}

        var resObjList = [];

        _.forOwn(photos, function(value, key) {
            var resObj = convertToPhotoListObject(paramOrderNo, value);
            resObjList.push(resObj);
        });

        return res.status(200).json(resObjList);
    });
};

exports.createPhoto = function(req, res) {
    var paramOrderNo = parseInt(req.param('orderNo'));
    if(!paramOrderNo) { return res.status(404).json({"message": "You need to pass an order number"}); }

    var size = 0;
    var data = new Buffer('');

    req.on('data', function(chunk) {
        data = Buffer.concat([data, chunk]);
        size += chunk.length;
        console.log('Chunk: ' + chunk.length + ' total: ' + size);
    });
    req.on('end', function() {
        req.rawBody = data;

        var photo = new Photo;
        photo.orderNo = paramOrderNo;
        photo.data = data;
        photo.contentType = "image/jpeg";
        photo.save(function (err, photo) {
            if (err) { return handleError(res, err); }

            res.contentType(photo.contentType);
            var link = "/order/" + paramOrderNo + "/photos/" + photo._id;
            console.log(link);

            //send photo
            //return res.status(200).send(photo.data);

            //send link to photo
            res.header("Content-Type", "application/json");
            return res.status(200).send({"photoPath": link});
        });

    });

};

exports.showPhotoById = function(req, res) {
    var paramOrderNo = parseInt(req.param('orderNo'));
    var paramPhotoId = req.param('photoId');
    if(!paramOrderNo) { return res.status(404).json({"message": "You need to pass an order number"}); }
    if(!paramPhotoId) { return res.status(404).json({"message": "You need to pass a photoId"}); }

    Photo.findOne({ orderNo: paramOrderNo, _id: paramPhotoId }, function(err, photo) {
        if(err) { return handleError(res, err); }
        if(!photo) {return res.status(404).json({"message": "Photo not found"});}

        res.header("Content-Type", photo.contentType);
        return res.status(200).send(photo.data);
    });

};

function convertToPhotoListObject(orderNo, mongooseObj) {
    var obj = {};

    obj.id = 1 + Math.floor((Math.random() * 10000) + 1);;
    obj.photoPath = "http://" + config.ip + ':' + config.port + '/orders/' + orderNo + '/photos/' + mongooseObj._id;

    return obj;
}


//======================================================================================================================
//===== Converters
//======================================================================================================================


function handleError(res, err) {
    console.log(JSON.stringify(err));
    return res.status(500).json({ "message": err.message });
}