'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var BatchSchema = new Schema({
    // header
    _id: Number,
    dealerNo: {type: Number, index: true},
    branchNo: {type: Number, index: true},
    batchNo: {type: String, index: true},
    creationDate: String,
    countingDate: String,
    totalLines: Number,
    checkedLines: Number
});

// Ensure virtual fields are serialised.
BatchSchema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model('Batch', BatchSchema);

