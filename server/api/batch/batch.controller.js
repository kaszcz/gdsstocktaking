'use strict';

var config = require('../../config/environment');

var _ = require('lodash');
var moment = require('../../../node_modules/moment/moment');

var Batch = require('./batch.model');
var Part = require('../part/part.model');
var Prepick = require('../prepick/prepick.model');

//======================================================================================================================
//===== Batches
//======================================================================================================================
exports.index = function(req, res) {
    var paramDealerNo = req.param('dealerNo');
    var paramBranchNo = req.param('branchNo');
    var paramStatus = req.param('status');
    if(!paramDealerNo) { return res.status(404).json({"message": "You need to pass a dealer number"}); }
    if(!paramBranchNo) { return res.status(404).json({"message": "You need to pass a branch number"}); }

    if(!paramStatus) {
        var resObjList = [];

        Batch.find({dealerNo: paramDealerNo, branchNo: paramBranchNo}, function (err, orders) {
            if(err) { return handleError(res, err); }
            _.forOwn(orders, function(value, key) {
                var resObj = convertToBatchListObject(value);
                resObjList.push(resObj);
            });
            return res.status(200).json(resObjList);
        });
    } else if(paramStatus == "active") {
        var resObjList = [];
        return res.status(404).json({"message": "Batches not found"});
    } else {
        return res.json(404, {"message": "Wrong status!"});
    }

};


exports.show = function(req, res) {
    var paramDealerNo = req.param('dealerNo');
    var paramBranchNo = req.param('branchNo');
    var paramBatchNo = req.param('batchNo');
    if(!paramDealerNo) { return res.status(404).json({"message": "You need to pass a dealer number"}); }
    if(!paramBranchNo) { return res.status(404).json({"message": "You need to pass a branch number"}); }
    if(!paramBatchNo) { return res.status(404).json({"message": "You need to pass a batch number"}); }


    Batch.findOne({dealerNo: paramDealerNo, branchNo: paramBranchNo, batchNo: paramBatchNo}, function (err, batch) {
        if (err) { return handleError(res, err); }
        if (!batch) { return res.status(404).json({"message": "Batch not found!"}); }

        var resObj = convertToBatchListObject(batch);
        return res.status(200).json(resObj);
    });
}; //show

function convertToBatchListObject(mongooseObj) {
    var obj = {};
    //obj.id = mongooseObj._id;
    obj.dealerNo = mongooseObj.dealerNo;
    obj.branchNo = mongooseObj.branchNo;
    obj.batchNo = mongooseObj.batchNo;
    obj.creationDate = mongooseObj.creationDate;
    obj.countingDate = mongooseObj.countingDate;
    obj.totalLines =  mongooseObj.totalLines;
    obj.checkedLines = mongooseObj.checkedLines;
    return obj;
}


//======================================================================================================================
//===== Parts
//======================================================================================================================
exports.showParts = function(req, res) {
    var paramDealerNo = req.param('dealerNo');
    var paramBranchNo = req.param('branchNo');
    var paramBatchNo = req.param('batchNo');
    if(!paramDealerNo) { return res.status(404).json({"message": "You need to pass a dealer number"}); }
    if(!paramBranchNo) { return res.status(404).json({"message": "You need to pass a branch number"}); }
    if(!paramBatchNo) { return res.status(404).json({"message": "You need to pass a batch number"}); }

    var status = req.param('status');

    var resObjList = [];

    Batch.findOne({dealerNo: paramDealerNo, branchNo: paramBranchNo, batchNo: paramBatchNo}, function (err, batch) {
        if (err) { return handleError(res, err); }
        if (!batch) { return res.status(404).json({"message": "Batch not found!"}); }


        if(!status||status==="unchecked") {
            Part.find({dealerNo: paramDealerNo, branchNo: paramBranchNo, batchNo: paramBatchNo, status: "UNCHECKED"}, function (err, parts) {
                if(err) { return handleError(res, err); }
                if(!parts) { return res.status(404).json({"message": "Part not found!"}); }

                _.forOwn(parts, function(value, key) {
                    var resObj = convertToPartListObject(value);
                    resObjList.push(resObj);
                });
                return res.status(200).json(resObjList);
            });
        } else if(status == "all") {
            Part.find({dealerNo: paramDealerNo, branchNo: paramBranchNo, batchNo: paramBatchNo}, function (err, parts) {
                if(err) { return handleError(res, err); }
                if(!parts) { return res.status(404).json({"message": "Part not found!"}); }

                _.forOwn(parts, function(value, key) {
                    var resObj = convertToPartListObject(value);
                    resObjList.push(resObj);
                });
                return res.status(200).json(resObjList);
            });
        } else {
            return res.status(404).json({"message": "Wrong status!"});
        }




    });

};


exports.updatePart = function(req, res) {
    // sanitize input
    var paramDealerNo = req.param('dealerNo');
    var paramBranchNo = req.param('branchNo');
    var paramBatchNo = req.param('batchNo');
    var paramLineNo = req.body.lineNo;
    if(!paramDealerNo) { return res.status(404).json({"message": "You need to pass a dealer number"}); }
    if(!paramBranchNo) { return res.status(404).json({"message": "You need to pass a branch number"}); }
    if(!paramBatchNo) { return res.status(404).json({"message": "You need to pass a batch number"}); }
    if(!paramLineNo) { return res.status(404).json({"message": "You need to pass a line number"}); }

    Batch.findOne({dealerNo: paramDealerNo, branchNo: paramBranchNo, batchNo: paramBatchNo}, function (err, batch) {
        if (err) { return handleError(res, err); }
        if (!batch) { return res.status(404).json({"message": "Batch not found!"}); }

        Part.findOne({dealerNo: paramDealerNo, branchNo: paramBranchNo, batchNo: paramBatchNo, lineNo: paramLineNo }, function (err, part) {
            if (err) { return handleError(res, err); }
            if(!part) { return res.status(404).json({"message": "Part not found!"}); }

            var updated = _.merge(part, req.body);
            updated.save(function (err) {
                if (err) { return handleError(res, err); }
                var resObj = convertToPartListObject(part);
                return res.json(200, resObj);
            });
        });

    });


};


function convertToPartListObject(part) {
    var obj = {};

    obj.dealerNo = part.dealerNo;
    obj.branchNo = part.branchNo;
    obj.batchNo = part.batchNo;
    obj.lineNo = part.lineNo;
    obj.partPrefix = part.partPrefix;
    obj.partNo = part.partNo;
    obj.description = part.description;
    obj.location = part.location;
    obj.status = part.status;
    obj.countedQty = part.countedQty;
    obj.theoreticalQty = part.theoreticalQty;
    obj.prepickQty = part.prepickQty;

    return obj;
}


//======================================================================================================================
//===== Prepick Details
//======================================================================================================================
exports.showPrepickDetails = function(req, res) {
    var paramDealerNo = req.param('dealerNo');
    var paramBranchNo = req.param('branchNo');
    var paramPartPrefix = req.param('partPrefix');
    var paramPartNo = req.param('partNo');
    if(!paramDealerNo) { return res.status(404).json({"message": "You need to pass a dealer number"}); }
    if(!paramBranchNo) { return res.status(404).json({"message": "You need to pass a branch number"}); }
    if(!paramPartPrefix) { return res.status(404).json({"message": "You need to pass a part prefix number"}); }
    if(!paramPartNo) { return res.status(404).json({"message": "You need to pass a part number branch number"}); }

    var resObjList = [];

    Prepick.find({partPrefix: paramPartPrefix, partNo: paramPartNo}, function (err, prepicks) {
        if(err) { return handleError(res, err); }
        if(!prepicks || prepicks.length == 0) { return res.status(404).json({"message": "Prepick info not found!"}); }

        _.forOwn(prepicks, function(value, key) {
            var resObj = convertToPrepickListObject(value);
            resObjList.push(resObj);
        });
        return res.status(200).json(resObjList);
    });


};

function convertToPrepickListObject(prepick) {
    var obj = {};

    obj.location = prepick.location;
    obj.quantity = prepick.quantity;

    return obj;
}

//======================================================================================================================
//===== Parameters
//======================================================================================================================
exports.showParameters = function(req, res) {
    var paramType = req.param('type');

    var obj = {
        showTheoreticalQty: true
    };

    if(paramType == "mobile") {
        return res.status(200).json(obj);
    } else {
        return res.status(404).json({"message": "Parameter=type not found or not supported!"});
    }

};

//======================================================================================================================
//===== Branches
//======================================================================================================================
exports.showBranches = function(req, res) {
    var list = [
        {branchNo: 1, branchName: "Group Truck Center TRIBUSWINKE"},
        {branchNo: 2, branchName: "Group Truck Center KORNEUBURG"},
        {branchNo: 3, branchName: "Group Truck Center PREMSTSSTTEN"},
        {branchNo: 4, branchName: "Group Truck Center WEIDKIRCHEN"},
        {branchNo: 5, branchName: "Group Truck Center THAUR"},
        {branchNo: 5, branchName: "Group Truck Center TRIBUSWINKE"},
        {branchNo: 6, branchName: "Group Truck Center KORNEUBURG"},
        {branchNo: 7, branchName: "Group Truck Center PREMSTSSTTEN"},
        {branchNo: 8, branchName: "Group Truck Center WEIDKIRCHEN"},
        {branchNo: 9, branchName: "Group Truck Center THAUR"},
        {branchNo: 10, branchName: "Group Truck Center TRIBUSWINKE"},
        {branchNo: 12, branchName: "Group Truck Center KORNEUBURG"},
        {branchNo: 13, branchName: "Group Truck Center PREMSTSSTTEN"},
        {branchNo: 14, branchName: "Group Truck Center WEIDKIRCHEN"},
        {branchNo: 15, branchName: "Group Truck Center THAUR"},
        {branchNo: 16, branchName: "Group Truck Center TRIBUSWINKE"},
        {branchNo: 17, branchName: "Group Truck Center KORNEUBURG"},
        {branchNo: 18, branchName: "Group Truck Center PREMSTSSTTEN"},
        {branchNo: 19, branchName: "Group Truck Center WEIDKIRCHEN"},
        {branchNo: 20, branchName: "Group Truck Center THAUR"}
    ];


    return res.status(200).json(list);
};

//======================================================================================================================
//===== Config
//======================================================================================================================
exports.showConfig = function(req, res) {
    var list = [
        {
            countryCode: "pl",
            countryName: "Poland",
            levels: [
                {
                    name: "Production",
                    dealers: [
                        {dealerName: "Volvo Truck Center", dealerNo: 1},
                        {dealerName: "Intruck", dealerNo: 2},
                        {dealerName: "Aldo", dealerNo: 3},
                        {dealerName: "Skrzypa", dealerNo: 4},
                        {dealerName: "Dzida", dealerNo: 5},
                        {dealerName: "Galauto", dealerNo: 6},
                        {dealerName: "Autosfera", dealerNo: 7},
                        {dealerName: "Omega", dealerNo: 8},
                        {dealerName: "Euro-Bial", dealerNo: 9},
                        {dealerName: "Nijhof-Wassnik", dealerNo: 10},
                        {dealerName: "Eurocomplex", dealerNo: 11},
                        {dealerName: "Tandem", dealerNo: 12},
                        {dealerName: "Retos", dealerNo: 13},
                        {dealerName: "Polsad", dealerNo: 14},
                        {dealerName: "Wimax", dealerNo: 15},
                        {dealerName: "Omni Moto", dealerNo: 16},
                        {dealerName: "Gab-Trans", dealerNo: 17}
                    ]
                },
                {
                    name: "Customer Acceptance (QA)",
                    dealers: [
                        {dealerName: "MR1642 QA Maintenance Release", dealerNo: 1},
                        {dealerName: "MR1705 QA Maintenance Release", dealerNo: 2},
                    ]
                }
            ]
        },
        {
            countryCode: "fi",
            countryName: "Finland",
            levels: [
                {
                    name: "Production",
                    dealers: [
                        {dealerName: "VTC", dealerNo: 1},
                        {dealerName: "HF Autohold", dealerNo: 2},
                        {dealerName: "Raskaspari", dealerNo: 3},
                        {dealerName: "Wetteri", dealerNo: 4},
                        {dealerName: "Auto-Kilta Trucks", dealerNo: 5},
                        {dealerName: "Kayttoauto", dealerNo: 6}
                    ]
                },
                {
                    name: "Customer Acceptance (QA)",
                    dealers: [
                        {dealerName: "MR1642 QA Maintenance Release", dealerNo: 1},
                        {dealerName: "MR1705 QA Maintenance Release", dealerNo: 2},
                    ]
                },
                {
                    name: "Test CA",
                    dealers: [
                        {dealerName: "FI - C1 SAP TEST", dealerNo: 1},
                        {dealerName: "FI - C2 TEST", dealerNo: 2},
                        {dealerName: "FI - Education", dealerNo: 3},
                        {dealerName: "FI - Sandbox", dealerNo: 4},
                    ]
                }
            ]
        },
        {
            countryCode: "at",
            countryName: "Austra",
            levels: [
                {
                    name: "Production",
                    dealers: [
                        {dealerName: "Volvo Truck Center", dealerNo: 1},
                        {dealerName: "Intruck", dealerNo: 2},
                        {dealerName: "Aldo", dealerNo: 3},
                        {dealerName: "Skrzypa", dealerNo: 4},
                        {dealerName: "Dzida", dealerNo: 5},
                        {dealerName: "Galauto", dealerNo: 6},
                        {dealerName: "Autosfera", dealerNo: 7},
                        {dealerName: "Omega", dealerNo: 8},
                        {dealerName: "Euro-Bial", dealerNo: 9},
                        {dealerName: "Nijhof-Wassnik", dealerNo: 10},
                        {dealerName: "Eurocomplex", dealerNo: 11},
                        {dealerName: "Tandem", dealerNo: 12},
                        {dealerName: "Retos", dealerNo: 13},
                        {dealerName: "Polsad", dealerNo: 14},
                        {dealerName: "Wimax", dealerNo: 15},
                        {dealerName: "Omni Moto", dealerNo: 16},
                        {dealerName: "Gab-Trans", dealerNo: 17}
                    ]
                },
                {
                    name: "Customer Acceptance (QA)",
                    dealers: [
                        {dealerName: "MR1642 QA Maintenance Release", dealerNo: 1},
                        {dealerName: "MR1705 QA Maintenance Release", dealerNo: 2},
                        {dealerName: "FI - C1 SAP TEST", dealerNo: 3},
                        {dealerName: "FI - C2 TEST", dealerNo: 4},
                        {dealerName: "FI - Education", dealerNo: 5},
                        {dealerName: "FI - Sandbox", dealerNo: 6},
                    ]
                }
            ]
        },
        {
            countryCode: "be",
            countryName: "Belgium",
            levels: [
                {
                    name: "Production",
                    dealers: [
                        {dealerName: "Volvo Truck Center", dealerNo: 1},
                        {dealerName: "Intruck", dealerNo: 2},
                        {dealerName: "Aldo", dealerNo: 3},
                        {dealerName: "Skrzypa", dealerNo: 4},
                        {dealerName: "Dzida", dealerNo: 5},
                        {dealerName: "Galauto", dealerNo: 6},
                        {dealerName: "Autosfera", dealerNo: 7},
                        {dealerName: "Omega", dealerNo: 8},
                        {dealerName: "Euro-Bial", dealerNo: 9},
                        {dealerName: "Nijhof-Wassnik", dealerNo: 10},
                        {dealerName: "Eurocomplex", dealerNo: 11},
                        {dealerName: "Tandem", dealerNo: 12},
                        {dealerName: "Retos", dealerNo: 13},
                        {dealerName: "Polsad", dealerNo: 14},
                        {dealerName: "Wimax", dealerNo: 15},
                        {dealerName: "Omni Moto", dealerNo: 16},
                        {dealerName: "Gab-Trans", dealerNo: 17}
                    ]
                },
                {
                    name: "Customer Acceptance (QA)",
                    dealers: [
                        {dealerName: "MR1642 QA Maintenance Release", dealerNo: 1},
                        {dealerName: "MR1705 QA Maintenance Release", dealerNo: 2},
                        {dealerName: "FI - C1 SAP TEST", dealerNo: 3},
                        {dealerName: "FI - C2 TEST", dealerNo: 4},
                        {dealerName: "FI - Education", dealerNo: 5},
                        {dealerName: "FI - Sandbox", dealerNo: 6},
                    ]
                }
            ]
        },
        {
            countryCode: "cz",
            countryName: "Czech Republic",
            levels: [
                {
                    name: "Production",
                    dealers: [
                        {dealerName: "Volvo Truck Center", dealerNo: 1},
                        {dealerName: "Intruck", dealerNo: 2},
                        {dealerName: "Aldo", dealerNo: 3},
                        {dealerName: "Skrzypa", dealerNo: 4},
                        {dealerName: "Dzida", dealerNo: 5},
                        {dealerName: "Galauto", dealerNo: 6},
                        {dealerName: "Autosfera", dealerNo: 7},
                        {dealerName: "Omega", dealerNo: 8},
                        {dealerName: "Euro-Bial", dealerNo: 9},
                        {dealerName: "Nijhof-Wassnik", dealerNo: 10},
                        {dealerName: "Eurocomplex", dealerNo: 11},
                        {dealerName: "Tandem", dealerNo: 12},
                        {dealerName: "Retos", dealerNo: 13},
                        {dealerName: "Polsad", dealerNo: 14},
                        {dealerName: "Wimax", dealerNo: 15},
                        {dealerName: "Omni Moto", dealerNo: 16},
                        {dealerName: "Gab-Trans", dealerNo: 17}
                    ]
                },
                {
                    name: "Customer Acceptance (QA)",
                    dealers: [
                        {dealerName: "MR1642 QA Maintenance Release", dealerNo: 1},
                        {dealerName: "MR1705 QA Maintenance Release", dealerNo: 2},
                        {dealerName: "FI - C1 SAP TEST", dealerNo: 3},
                        {dealerName: "FI - C2 TEST", dealerNo: 4},
                        {dealerName: "FI - Education", dealerNo: 5},
                        {dealerName: "FI - Sandbox", dealerNo: 6},
                    ]
                }
            ]
        },
        {
            countryCode: "de",
            countryName: "Germany",
            levels: [
                {
                    name: "Production",
                    dealers: [
                        {dealerName: "Volvo Truck Center", dealerNo: 1},
                        {dealerName: "Intruck", dealerNo: 2},
                        {dealerName: "Aldo", dealerNo: 3},
                        {dealerName: "Skrzypa", dealerNo: 4},
                        {dealerName: "Dzida", dealerNo: 5},
                        {dealerName: "Galauto", dealerNo: 6},
                        {dealerName: "Autosfera", dealerNo: 7},
                        {dealerName: "Omega", dealerNo: 8},
                        {dealerName: "Euro-Bial", dealerNo: 9},
                        {dealerName: "Nijhof-Wassnik", dealerNo: 10},
                        {dealerName: "Eurocomplex", dealerNo: 11},
                        {dealerName: "Tandem", dealerNo: 12},
                        {dealerName: "Retos", dealerNo: 13},
                        {dealerName: "Polsad", dealerNo: 14},
                        {dealerName: "Wimax", dealerNo: 15},
                        {dealerName: "Omni Moto", dealerNo: 16},
                        {dealerName: "Gab-Trans", dealerNo: 17}
                    ]
                },
                {
                    name: "Customer Acceptance (QA)",
                    dealers: [
                        {dealerName: "MR1642 QA Maintenance Release", dealerNo: 1},
                        {dealerName: "MR1705 QA Maintenance Release", dealerNo: 2},
                        {dealerName: "FI - C1 SAP TEST", dealerNo: 3},
                        {dealerName: "FI - C2 TEST", dealerNo: 4},
                        {dealerName: "FI - Education", dealerNo: 5},
                        {dealerName: "FI - Sandbox", dealerNo: 6},
                    ]
                }
            ]
        },
        {
            countryCode: "dk",
            countryName: "Denmark",
            levels: [
                {
                    name: "Production",
                    dealers: [
                        {dealerName: "Volvo Truck Center", dealerNo: 1},
                        {dealerName: "Intruck", dealerNo: 2},
                        {dealerName: "Aldo", dealerNo: 3},
                        {dealerName: "Skrzypa", dealerNo: 4},
                        {dealerName: "Dzida", dealerNo: 5},
                        {dealerName: "Galauto", dealerNo: 6},
                        {dealerName: "Autosfera", dealerNo: 7},
                        {dealerName: "Omega", dealerNo: 8},
                        {dealerName: "Euro-Bial", dealerNo: 9},
                        {dealerName: "Nijhof-Wassnik", dealerNo: 10},
                        {dealerName: "Eurocomplex", dealerNo: 11},
                        {dealerName: "Tandem", dealerNo: 12},
                        {dealerName: "Retos", dealerNo: 13},
                        {dealerName: "Polsad", dealerNo: 14},
                        {dealerName: "Wimax", dealerNo: 15},
                        {dealerName: "Omni Moto", dealerNo: 16},
                        {dealerName: "Gab-Trans", dealerNo: 17}
                    ]
                },
                {
                    name: "Customer Acceptance (QA)",
                    dealers: [
                        {dealerName: "MR1642 QA Maintenance Release", dealerNo: 1},
                        {dealerName: "MR1705 QA Maintenance Release", dealerNo: 2},
                        {dealerName: "FI - C1 SAP TEST", dealerNo: 3},
                        {dealerName: "FI - C2 TEST", dealerNo: 4},
                        {dealerName: "FI - Education", dealerNo: 5},
                        {dealerName: "FI - Sandbox", dealerNo: 6},
                    ]
                }
            ]
        },
        {
            countryCode: "ee",
            countryName: "Estonia",
            levels: [
                {
                    name: "Production",
                    dealers: [
                        {dealerName: "Volvo Truck Center", dealerNo: 1},
                        {dealerName: "Intruck", dealerNo: 2},
                        {dealerName: "Aldo", dealerNo: 3},
                        {dealerName: "Skrzypa", dealerNo: 4},
                        {dealerName: "Dzida", dealerNo: 5},
                        {dealerName: "Galauto", dealerNo: 6},
                        {dealerName: "Autosfera", dealerNo: 7},
                        {dealerName: "Omega", dealerNo: 8},
                        {dealerName: "Euro-Bial", dealerNo: 9},
                        {dealerName: "Nijhof-Wassnik", dealerNo: 10},
                        {dealerName: "Eurocomplex", dealerNo: 11},
                        {dealerName: "Tandem", dealerNo: 12},
                        {dealerName: "Retos", dealerNo: 13},
                        {dealerName: "Polsad", dealerNo: 14},
                        {dealerName: "Wimax", dealerNo: 15},
                        {dealerName: "Omni Moto", dealerNo: 16},
                        {dealerName: "Gab-Trans", dealerNo: 17}
                    ]
                },
                {
                    name: "Customer Acceptance (QA)",
                    dealers: [
                        {dealerName: "MR1642 QA Maintenance Release", dealerNo: 1},
                        {dealerName: "MR1705 QA Maintenance Release", dealerNo: 2},
                        {dealerName: "FI - C1 SAP TEST", dealerNo: 3},
                        {dealerName: "FI - C2 TEST", dealerNo: 4},
                        {dealerName: "FI - Education", dealerNo: 5},
                        {dealerName: "FI - Sandbox", dealerNo: 6},
                    ]
                }
            ]
        },
        {
            countryCode: "fr",
            countryName: "France",
            levels: [
                {
                    name: "Production",
                    dealers: [
                        {dealerName: "Volvo Truck Center", dealerNo: 1},
                        {dealerName: "Intruck", dealerNo: 2},
                        {dealerName: "Aldo", dealerNo: 3},
                        {dealerName: "Skrzypa", dealerNo: 4},
                        {dealerName: "Dzida", dealerNo: 5},
                        {dealerName: "Galauto", dealerNo: 6},
                        {dealerName: "Autosfera", dealerNo: 7},
                        {dealerName: "Omega", dealerNo: 8},
                        {dealerName: "Euro-Bial", dealerNo: 9},
                        {dealerName: "Nijhof-Wassnik", dealerNo: 10},
                        {dealerName: "Eurocomplex", dealerNo: 11},
                        {dealerName: "Tandem", dealerNo: 12},
                        {dealerName: "Retos", dealerNo: 13},
                        {dealerName: "Polsad", dealerNo: 14},
                        {dealerName: "Wimax", dealerNo: 15},
                        {dealerName: "Omni Moto", dealerNo: 16},
                        {dealerName: "Gab-Trans", dealerNo: 17}
                    ]
                },
                {
                    name: "Customer Acceptance (QA)",
                    dealers: [
                        {dealerName: "MR1642 QA Maintenance Release", dealerNo: 1},
                        {dealerName: "MR1705 QA Maintenance Release", dealerNo: 2},
                        {dealerName: "FI - C1 SAP TEST", dealerNo: 3},
                        {dealerName: "FI - C2 TEST", dealerNo: 4},
                        {dealerName: "FI - Education", dealerNo: 5},
                        {dealerName: "FI - Sandbox", dealerNo: 6},
                    ]
                }
            ]
        },
        {
            countryCode: "gb",
            countryName: "Great Britan",
            levels: [
                {
                    name: "Production",
                    dealers: [
                        {dealerName: "Volvo Truck Center", dealerNo: 1},
                        {dealerName: "Intruck", dealerNo: 2},
                        {dealerName: "Aldo", dealerNo: 3},
                        {dealerName: "Skrzypa", dealerNo: 4},
                        {dealerName: "Dzida", dealerNo: 5},
                        {dealerName: "Galauto", dealerNo: 6},
                        {dealerName: "Autosfera", dealerNo: 7},
                        {dealerName: "Omega", dealerNo: 8},
                        {dealerName: "Euro-Bial", dealerNo: 9},
                        {dealerName: "Nijhof-Wassnik", dealerNo: 10},
                        {dealerName: "Eurocomplex", dealerNo: 11},
                        {dealerName: "Tandem", dealerNo: 12},
                        {dealerName: "Retos", dealerNo: 13},
                        {dealerName: "Polsad", dealerNo: 14},
                        {dealerName: "Wimax", dealerNo: 15},
                        {dealerName: "Omni Moto", dealerNo: 16},
                        {dealerName: "Gab-Trans", dealerNo: 17}
                    ]
                },
                {
                    name: "Customer Acceptance (QA)",
                    dealers: [
                        {dealerName: "MR1642 QA Maintenance Release", dealerNo: 1},
                        {dealerName: "MR1705 QA Maintenance Release", dealerNo: 2},
                        {dealerName: "FI - C1 SAP TEST", dealerNo: 3},
                        {dealerName: "FI - C2 TEST", dealerNo: 4},
                        {dealerName: "FI - Education", dealerNo: 5},
                        {dealerName: "FI - Sandbox", dealerNo: 6},
                    ]
                }
            ]
        },
        {
            countryCode: "it",
            countryName: "Italy",
            levels: [
                {
                    name: "Production",
                    dealers: [
                        {dealerName: "Volvo Truck Center", dealerNo: 1},
                        {dealerName: "Intruck", dealerNo: 2},
                        {dealerName: "Aldo", dealerNo: 3},
                        {dealerName: "Skrzypa", dealerNo: 4},
                        {dealerName: "Dzida", dealerNo: 5},
                        {dealerName: "Galauto", dealerNo: 6},
                        {dealerName: "Autosfera", dealerNo: 7},
                        {dealerName: "Omega", dealerNo: 8},
                        {dealerName: "Euro-Bial", dealerNo: 9},
                        {dealerName: "Nijhof-Wassnik", dealerNo: 10},
                        {dealerName: "Eurocomplex", dealerNo: 11},
                        {dealerName: "Tandem", dealerNo: 12},
                        {dealerName: "Retos", dealerNo: 13},
                        {dealerName: "Polsad", dealerNo: 14},
                        {dealerName: "Wimax", dealerNo: 15},
                        {dealerName: "Omni Moto", dealerNo: 16},
                        {dealerName: "Gab-Trans", dealerNo: 17}
                    ]
                },
                {
                    name: "Customer Acceptance (QA)",
                    dealers: [
                        {dealerName: "MR1642 QA Maintenance Release", dealerNo: 1},
                        {dealerName: "MR1705 QA Maintenance Release", dealerNo: 2},
                        {dealerName: "FI - C1 SAP TEST", dealerNo: 3},
                        {dealerName: "FI - C2 TEST", dealerNo: 4},
                        {dealerName: "FI - Education", dealerNo: 5},
                        {dealerName: "FI - Sandbox", dealerNo: 6},
                    ]
                }
            ]
        }
    ];

    return res.status(200).json(list);

};

//======================================================================================================================
//===== Converters
//======================================================================================================================


function handleError(res, err) {
    console.log(JSON.stringify(err));
    return res.status(500).json({ "message": err.message });
}