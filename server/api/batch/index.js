/**
 * Created by kaszcz on 06/10/16.
 */
var express = require('express');
var controller = require('./batch.controller');

var router = express.Router();

router.get('/:countryCode/:dealerNo/:branchNo/batches', controller.index);
router.get('/:countryCode/:dealerNo/:branchNo/batches/:batchNo', controller.show);

router.get('/:countryCode/:dealerNo/:branchNo/batches/:batchNo/lines', controller.showParts);
router.put('/:countryCode/:dealerNo/:branchNo/batches/:batchNo/lines', controller.updatePart);

router.get('/:countryCode/:dealerNo/:branchNo/parts/:partPrefix/:partNo/prepick', controller.showPrepickDetails);

router.get('/:countryCode/:dealerNo/:branchNo/parameters', controller.showParameters);

router.get('/:countryCode/:dealerNo/branches', controller.showBranches);

router.get('/config', controller.showConfig);

module.exports = router;