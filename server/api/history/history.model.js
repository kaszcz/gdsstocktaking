'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var HistorySchema = new Schema({
    // header
    _id: Number,
    orderNo: Number,
    MAT: String,
    orderStatus: String,
    failure: String,
    customerName: String,
    contactNumber: String,
    model: String,
    machineLocation: String,
    machineSN: String,
    createdDate: Number,
    plannedDate: Number,
    closedDate: Number,
    HMR: Number,
    breakDown: Boolean,
    mechanic: String,
});

// Duplicate the ID field.
//OrderSchema.virtual('id').get(function() {
//    var id = mongoose.Types.ObjectId(this._id);
//    return parseInt(this._id);
//});

// Ensure virtual fields are serialised.
HistorySchema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model('History', HistorySchema);

