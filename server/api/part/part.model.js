'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PartSchema = new Schema({
    _id: Number,
    dealerNo: {type: Number, index: true},
    branchNo: {type: Number, index: true},
    batchNo: {type: String, index: true},
    lineNo: {type: Number, index: true},
    partPrefix: String,
    partNo: String,
    description: String,
    location: String,
    status: String,
    countedQty: Number,
    theoreticalQty: Number,
    prepickQty: Number
});

// Ensure virtual fields are serialised.
PartSchema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model('Part', PartSchema);
