/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var Thing = require('../api/thing/thing.model');
var User = require('../api/user/user.model');

var Batch = require('../api/batch/batch.model');
var Part = require('../api/part/part.model');
var Prepick = require('../api/prepick/prepick.model');


Batch.find({}).remove(function() {
    Batch.create({
            _id: 1,
            dealerNo: 22664,
            branchNo: 2,
            batchNo: "00047A",
            creationDate: "2016-10-03",
            countingDate: "2016-10-06",
            totalLines: 123,
            checkedLines: 44
        },
        {
            _id: 2,
            dealerNo: 22664,
            branchNo: 2,
            batchNo: "000158",
            creationDate: "2016-09-01",
            countingDate: "2016-10-02",
            totalLines: 523,
            checkedLines: 143
        },
        {
            _id: 3,
            dealerNo: 22664,
            branchNo: 2,
            batchNo: "000122",
            creationDate: "2016-05-23",
            countingDate: "2016-05-27",
            totalLines: 39,
            checkedLines: 0
        },
        {
            _id: 4,
            dealerNo: 22664,
            branchNo: 2,
            batchNo: "000125",
            creationDate: "2016-05-23",
            countingDate: "2016-05-27",
            totalLines: 24,
            checkedLines: 4
        },
        {
            _id: 5,
            dealerNo: 22664,
            branchNo: 2,
            batchNo: "000128",
            creationDate: "2016-02-01",
            countingDate: "2016-02-05",
            totalLines: 43,
            checkedLines: 3
        },
        {
            _id: 6,
            dealerNo: 22664,
            branchNo: 2,
            batchNo: "000120",
            creationDate: "2016-12-28",
            countingDate: "2016-12-31",
            totalLines: 33,
            checkedLines: 30
        },
        {
            _id: 7,
            dealerNo: 22664,
            branchNo: 2,
            batchNo: "000002",
            creationDate: "2016-11-15",
            countingDate: "2016-11-17",
            totalLines: 78,
            checkedLines: 12
        },
        {
            _id: 8,
            dealerNo: 22664,
            branchNo: 2,
            batchNo: "000054",
            creationDate: "2016-11-01",
            countingDate: "2016-11-02",
            totalLines: 77,
            checkedLines: 13
        },
        {
            _id: 9,
            dealerNo: 22664,
            branchNo: 2,
            batchNo: "000980",
            creationDate: "2016-10-19",
            countingDate: "2016-10-25",
            totalLines: 134,
            checkedLines: 130
        },
        {
            _id: 10,
            dealerNo: 22664,
            branchNo: 2,
            batchNo: "000111",
            creationDate: "2016-07-09",
            countingDate: "2016-07-09",
            totalLines: 45,
            checkedLines: 12
        },
        {
            _id: 11,
            dealerNo: 22664,
            branchNo: 2,
            batchNo: "000567",
            creationDate: "2016-09-14",
            countingDate: "2016-10-02",
            totalLines: 78,
            checkedLines: 44
        },
        {
            _id: 12,
            dealerNo: 22664,
            branchNo: 2,
            batchNo: "000370",
            creationDate: "2016-09-19",
            countingDate: "2016-09-26",
            totalLines: 31,
            checkedLines: 0
        },
        function() {
            console.log('Finished populating Batches');
        });
});

Part.find({}).remove(function() {
    Part.create({
            _id: 1,
            dealerNo: 22664,
            branchNo: 2,
            batchNo: "00047A",
            lineNo: 1,
            partPrefix: "VO",
            partNo: "66365464",
            description: "Oil filter",
            location: "NEWDSP",
            status: "UNCHECKED",
            countedQty: 0,
            theoreticalQty: 15,
            prepickQty: 9
        },
        {
            _id: 2,
            dealerNo: 22664,
            branchNo: 2,
            batchNo: "00047A",
            lineNo: 2,
            partPrefix: "VO",
            partNo: "12345466",
            description: "Engine",
            location: "LOC123",
            status: "CONFIRMED",
            countedQty: 0,
            theoreticalQty: 13,
            prepickQty: 10
        },
        {
            _id: 3,
            dealerNo: 22664,
            branchNo: 2,
            batchNo: "00047A",
            lineNo: 3,
            partPrefix: "VO",
            partNo: "AGD12343",
            description: "Clutch",
            location: "XYZ678",
            status: "BLOCKED",
            countedQty: 0,
            theoreticalQty: 138,
            prepickQty: 0
        },
        {
            _id: 4,
            dealerNo: 22664,
            branchNo: 2,
            batchNo: "00047A",
            lineNo: 4,
            partPrefix: "VO",
            partNo: "34454253223",
            description: "Front Brake System",
            location: "FRY143",
            status: "ADJUSTED",
            countedQty: 0,
            theoreticalQty: 23,
            prepickQty: 1
        },
        {
            _id: 5,
            dealerNo: 22664,
            branchNo: 2,
            batchNo: "00047A",
            lineNo: 5,
            partPrefix: "VO",
            partNo: "32576876456324",
            description: "Front Wheel Ballbearing",
            location: "FRY143",
            status: "ADJUSTED",
            countedQty: 0,
            theoreticalQty: 33,
            prepickQty: 2
        },
        {
            _id: 6,
            dealerNo: 22664,
            branchNo: 2,
            batchNo: "00047A",
            lineNo: 6,
            partPrefix: "VO",
            partNo: "234345654",
            description: "Mordor entrance no 1",
            location: "MRD332",
            status: "UNCHECKED",
            countedQty: 0,
            theoreticalQty: 27,
            prepickQty: 2
        },
        {
            _id: 7,
            dealerNo: 22664,
            branchNo: 2,
            batchNo: "00047A",
            lineNo: 7,
            partPrefix: "VO",
            partNo: "0975324567654",
            description: "Front glass",
            location: "GTRSXF1",
            status: "ADJUSTED",
            countedQty: 0,
            theoreticalQty: 17,
            prepickQty: 2
        },
        {
            _id: 7,
            dealerNo: 22664,
            branchNo: 2,
            batchNo: "00047A",
            lineNo: 8,
            partPrefix: "VO",
            partNo: "222334345564345234234",
            description: "Part number can take 21 chars",
            location: "YUJ245",
            status: "UNCHECKED",
            countedQty: 0,
            theoreticalQty: 3,
            prepickQty: 1
        },
        function() {
            console.log('Finished populating Parts');
        });
});

Prepick.find({}).remove(function() {
    Prepick.create({
            _id: 1,
            dealerNo: 22664,
            branchNo: 2,
            partPrefix: "VO",
            partNo: "123456789",
            location: "ABC123",
            quantity: 3
        },
        {
            _id: 2,
            dealerNo: 22664,
            branchNo: 2,
            partPrefix: "VO",
            partNo: "123456789",
            location: "BIN231",
            quantity: 4
        },
        {
            _id: 3,
            dealerNo: 22664,
            branchNo: 2,
            partPrefix: "VO",
            partNo: "123456789",
            location: "BIN123",
            quantity: 1
        },
        {
            _id: 4,
            dealerNo: 22664,
            branchNo: 2,
            partPrefix: "VO",
            partNo: "123456789",
            location: "LOC689",
            quantity: 2
        },
        {
            _id: 5,
            dealerNo: 22664,
            branchNo: 2,
            partPrefix: "VO",
            partNo: "123456789",
            location: "LOC122",
            quantity: 3
        },
        {
            _id: 6,
            dealerNo: 22664,
            branchNo: 2,
            partPrefix: "VO",
            partNo: "123456789",
            location: "LOC423",
            quantity: 10
        },
        {
            _id: 7,
            dealerNo: 22664,
            branchNo: 2,
            partPrefix: "VO",
            partNo: "123456789",
            location: "XYZ343",
            quantity: 53
        },
        {
            _id: 8,
            dealerNo: 22664,
            branchNo: 2,
            partPrefix: "VO",
            partNo: "123456789",
            location: "BIN 113",
            quantity: 34
        },
        {
            _id: 9,
            dealerNo: 22664,
            branchNo: 2,
            partPrefix: "VO",
            partNo: "123456789",
            location: "LOC 12",
            quantity: 14
        },
        {
            _id: 10,
            dealerNo: 22664,
            branchNo: 2,
            partPrefix: "VO",
            partNo: "123456789",
            location: "ABC",
            quantity: 74
        },
        {
            _id: 11,
            dealerNo: 22664,
            branchNo: 2,
            partPrefix: "VO",
            partNo: "123456789",
            location: "GHY",
            quantity: 73
        },
        function() {
            console.log('Finished populating Prepick Details');
        });
});


User.find({}).remove(function() {
    User.create({
            provider: 'local',
            name: 'Test User',
            email: 'test@test.com',
            password: 'test'
        }, {
            provider: 'local',
            role: 'admin',
            name: 'Admin',
            email: 'admin@admin.com',
            password: 'admin'
        }, function() {
            console.log('Finished populating Users');
        }
    );
});