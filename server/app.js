/**
 * Main application file
 */

'use strict';

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var express = require('express');
var mongoose = require('mongoose');
var config = require('./config/environment');

//SSL
//var fs = require('fs');
//var http = require('http');
//var https = require('https');
//var privateKey  = fs.readFileSync('sslcert/server.key', 'utf8');
//var certificate = fs.readFileSync('sslcert/server.crt', 'utf8');

// Connect to database
mongoose.connect(config.mongo.uri, config.mongo.options);
//mongoose.createConnection(config.mongo.uri, config.mongo.options);
console.log("options=" + JSON.stringify(config.mongo.options));


// Populate DB with sample data
if(config.seedDB) { require('./config/seed'); }

// Setup server
var app = express();
var server = require('http').createServer(app);

//SSL
//var credentials = {key: privateKey, cert: certificate};
//var httpsServer = https.createServer(credentials, app);

require('./config/express')(app);
require('./routes')(app);

// Start http server
server.listen(config.port, config.ip, function () {
  console.log('HTTP Express server listening on %d, in %s mode', config.port, app.get('env'));
});

//SSL
//Start https server
//httpsServer.listen(443, config.ip, function() {
//  console.log('HTTPS Express server listening on %d, in %s mode', 443, app.get('env'));
//});

// Expose app
exports = module.exports = app;
